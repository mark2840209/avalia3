#!/bin/bash

change_username() {
    read -p "Digite o novo nome de usuário: " new_username
    PS1="$new_username: "
}

add_date_prompt() {
    PS1="[ \d \t \u ] $ "
}

add_directory_prompt() {
    PS1="[ \d \t \u\w ] $ "
}

change_color_prompt() {
    echo "Escolha uma cor:"
    echo "1. Preto"
    echo "2. Azul"
    echo "3. Verde"
    echo "4. Ciano"
    echo "5. Vermelho"
    echo "6. Castanho"
    echo "7. Roxo"

    read -p "Digite o número da cor desejada: " color_option

    case $color_option in
        1) PS1="\e[0;30m[\u@\h \W]\$ \e[m ";;
        2) PS1="\e[0;34m[\u@\h \W]\$ \e[m ";;
        3) PS1="\e[0;32m[\u@\h \W]\$ \e[m ";;
        4) PS1="\e[0;36m[\u@\h \W]\$ \e[m ";;
        5) PS1="\e[0;31m[\u@\h \W]\$ \e[m ";;
        6) PS1="\e[0;33m[\u@\h \W]\$ \e[m ";;
        7) PS1="\e[0;35m[\u@\h \W]\$ \e[m ";;
        *) echo "Opção inválida.";;
    esac
}

display_username() {
    echo $PS1
}

prompt_two_lines() {
    PS1="\n\[\033[1;34m\]\$ \[\033[0m\]"
}
