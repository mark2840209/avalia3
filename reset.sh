#!/bin/bash
# reset.sh

echo "Resetando prompt para a versão inicial..."
echo "PS1='\[\033[1;32m\]\u@\h\[\033[0m\]:\[\033[1;34m\]\w\[\033[0m\]\$ '" >> ~/.bashrc
source ~/.bashrc
