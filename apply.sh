#!/bin/bash
# apply.sh

source func.sh

echo "Aplicando customização..."
echo $PS1 >> ~/.bashrc
echo "export PS1='$PS1'" >> ~/.bashrc
echo "Para guardar as alterações, pressione Ctrl+O, Enter, Ctrl+X e execute 'source ~/.bashrc'."
