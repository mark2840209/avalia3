#!/bin/bash

source func.sh

echo "Escolha a opção desejada:"
echo "1. Mudar o nome do usuário"
echo "2. Adicionar data ao prompt"
echo "3. Adicionar diretório atual ao prompt"
echo "4. Mudança de cor no prompt"
echo "5. Exibir apenas o nome do usuário"
echo "6. Prompt em duas linhas"

read -p "Digite o número da opção desejada: " choice

case $choice in
    1) 
        change_username
        echo "PS1='$PS1'" > temp_script.sh
        ;;
    2)
        add_date_prompt
        echo "PS1='$PS1'" > temp_script.sh
        ;;
    3)
        add_directory_prompt
        echo "PS1='$PS1'" > temp_script.sh
        ;;
    4)
        change_color_prompt
        echo "PS1='$PS1'" > temp_script.sh
        ;;
    5)
        display_username
        ;;
    6)
        prompt_two_lines
        echo "PS1='$PS1'" > temp_script.sh
        ;;
    *)
        echo "Opção inválida."
        ;;
esac

if [[ $choice -ge 1 && $choice -le 6 ]]; then
    source temp_script.sh
    rm temp_script.sh
fi
